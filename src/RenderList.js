import React from "react";
import { List } from "@material-ui/core/"
import { ListItem } from "@material-ui/core/"
import { ListItemSecondaryAction } from "@material-ui/core/"
import { ListItemText } from "@material-ui/core/"
import { Checkbox } from "@material-ui/core/"
import { IconButton } from "@material-ui/core/"
import DeleteIcon from "@material-ui/icons/Delete";

const RenderList = (props) => {
	return (
		<List>
			{
				props.tasks.map((task, index) => {
					return (
						<ListItem button key={index} onClick={() => { props.toggleTaskDone(index) }}>
							<Checkbox checked={task.done}/>
							<ListItemText primary={task.value}/>
							<ListItemSecondaryAction>
								<IconButton onClick={() => { props.removeTask(index) }}>
									<DeleteIcon />
								</IconButton>
							</ListItemSecondaryAction>
						</ListItem>
					)
				})
			}
		</List>
	);
}

export default RenderList;
