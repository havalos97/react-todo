import React from "react";
import { Grid, Typography } from "@material-ui/core/"

import TodoForm from "./TodoForm.js";
import RenderList from "./RenderList.js";

/* Stateful */
// class App extends React.Component {
// constructor() {}
// willmount() {}
// didmount() {}
// ...
// render() {}
// willunmount() {}
// didunmount() {}
// }

/* Stateless */
// function App() {
// }

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			inputval: "",
			tasks: []
		};
	}

	handleSubmit = ((e) => {
		e.preventDefault();
		this.saveTask(this.state.inputval);
	})

	saveTask = ((task_content) => {
		if (task_content.trim()) {
			this.setState({
				inputval: "",
				tasks: [
					...this.state.tasks,
					{
						value: task_content,
						done: false
					}
				]
			});
		}
	});

	toggleTaskDone = ((index) => {
		this.setState({
			tasks: this.state.tasks.map((task, i) =>
				i === index ? {...task, done:!(task.done)} : task
			)
		})
	});

	removeTask = ((index) => {
		/* Imperativo */
		const tasks = [...this.state.tasks];
		tasks.splice(index, 1);
		this.setState({ tasks })

		/* Declarativo */
		this.setState({
			tasks: this.state.tasks.filter((_, i) => {
				return (i !== index)
			})
		})
	});

	handleChange = ((e) => {
		this.setState({
			inputval: e.target.value
		});
	})

	render() {
		return (
			<React.Fragment>
				{/* React Fragment is used instead of div to avoid having a lot of divs */}
				<Typography variant="h3" align="center" gutterBottom>
					To-Do List
				</Typography>
				<Grid container justify="center">
					<Grid item>
						<TodoForm
							handleChange={this.handleChange}
							inputval={this.state.inputval}
							handleSubmit={this.handleSubmit}
						/>
					</Grid>
				</Grid>
				<Grid container justify="center">
					<Grid item md={8}>
						<RenderList
							tasks={this.state.tasks}
							toggleTaskDone={this.toggleTaskDone}
							removeTask={this.removeTask}
						/>
					</Grid>
				</Grid>
			</React.Fragment>
		);
	}
}

export default App;
