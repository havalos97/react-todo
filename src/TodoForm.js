import React from "react";
import { TextField } from "@material-ui/core/"

const TodoForm = (props) => {
	return (
		<form onSubmit={props.handleSubmit}>
			<TextField
				label="To Do Task"
				type="text"
				placeholder="New task to do..."
				value={props.inputval}
				onChange={props.handleChange}
			/>
		</form>
	);
}

export default TodoForm;
